# Socket Tutorial
Make sure you have node.js installed. These docs should already have the proper node_modules needed to work with express (server intialization) and socket (client-server connections).

Get started starting the node.js server.
```
node server.js
```

After starting the server, open up index.html in Chrome (Double-click the file).

To see what's going on, open up the console with Command + Option + J (Mac) or Control + Shift + J (Windows).

After submitting something in the form, it should show up in BOTH the client-side (Chrome) and server-side (terminal) console outputs.

Submit a few more, then hit the "Get Messages" button. You should get a list of everything all clients have submitted with their timestamps.