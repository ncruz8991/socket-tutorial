/*
 * SERVER.JS
 * This is javascript that runs on the server side.
 * The server waits for client requests and responds accordingly.
 * Requests and responses are sent through socket.
 */

// Our 'database'
var messages = new Array();

// Initialize express
var express = require('express');
var app = express();

var server = app.listen(8080, function () {
  var host = server.address().address;
  var port = server.address().port;

  console.log('Example app listening at http://%s:%s', host, port);
});

var io = require('socket.io')(server);

io.on('connection', function (socket) {
	console.log("Connected with client!");

	// Handles: .submit-button click event
	socket.on('store-user-message', function (timestamp_in, message_in) {
		console.log("\nUser posted new message:");
		console.log(message_in);
		console.log("At time:");
		console.log(timestamp_in);

		// Initialize message (set up data to store)
		var thisMessage = {
		  timestamp: timestamp_in,
		  message: message_in
		}

		// "Store" it in the "database"
		messages.push(thisMessage);
	});

	// Handles: .get-messages-button click event
	socket.on('grab-all-messages', function () {
		// Send a message back to your client
		socket.emit('send-all-messages', messages);
	});
});