/*
 * CLIENT.JS
 * This is javascript that runs on the client side.
 * Any socket code here communicates with server.js (the node.js server).
 */

$(function() {

  // Set the serverURL
  var serverURL = 'http://localhost:8080/';

  // Initialize socket connection
  var socket = io(serverURL);

  // Add event handler to the submit-button click event
  $('.submit-button').click(function() {

    // Get current time
    var dt = new Date();
    var timestamp = dt.toUTCString();

    // Get what the user typed in the input form
    var message = $('.submit-input').val();

    // Log it in console to see what it is
    console.log("Submitting message to server:");
    console.log("  Timestamp: " + timestamp);
    console.log("    Message: " + message);

    // Send it to the server to store it.
    socket.emit('store-user-message', timestamp, message);
  });

  // Add event handler to the get-messages-button click event
  $(".get-messages-button").click(function() {

    // Clear the list of messages
    var list = $(".list-of-messages");
    list.empty();

    var appendHeaders = '<div class="message-wrapper">';
    appendHeaders += '<h4 class="timestamp">Timestamp</h4>';
    appendHeaders += '<h4 class="message">Message</h4>';
    appendHeaders += '</div>';

    list.append(appendHeaders);

    // Request all messages from the server
    socket.emit('grab-all-messages');
  });

  // Perform this code block whenever the server sends all messages
  socket.on('send-all-messages', function(messages) {
    console.log("Grabbing all messages from server...");
    console.log("List of messages: ");

    for (var i = 0; i < messages.length; ++i) {
      // Grab the data from messages
      var timestamp = messages[i].timestamp;
      var message = messages[i].message;

      console.log("  Timestamp: " + timestamp);
      console.log("    Message: " + message);

      // Format the data grabbed from the messages array
      var appendText = '<div class="message-wrapper">';
      appendText += '<p class="timestamp">' + timestamp + '</p>';
      appendText += '<p class="message">' + message + '</p>';
      appendText += '</div>';

      // For each message logged, append it to the .list-of-messages div
      $(".list-of-messages").append(appendText);
    }
  });
});